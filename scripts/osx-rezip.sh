#!/bin/bash

###########################################################################
# OS X Rezip Script
###########################################################################
# Recursively Scans For Any *.zip Files and Converts Them to Rar Files
# Requires Rar & Unzip To Be Installed
###########################################################################
# WARNING: Use at your own risk. This process can destructive. If you have
#   a rar and a zip in a directory by the same name, the zip can be deleted,
#   while the rar is kept.
###########################################################################
# Usage: ./rezip.sh Path [--dry-run]
#   Path: Path To Scan
#   Dry Run: Will Create the Rar, But Not Delete the Zips
###########################################################################
# Example: ./rezip.sh ~/ --dry-run
###########################################################################
# Methods

set -eu

# Defaults

extractionPath=/tmp/tf-extract
evictFiles=false
debugMode=false
scanPath=$(pwd)
keepFiles=true
isVerbose=false
includeCloud=false
overwriteExisting=false
fileExists=false
recursiveExtract=false
recompress=false
findOptions=(-name "*.zip")

calc() { awk "BEGIN{print $*}"; }

processFile() {
  actualPath "$1"
  file="$actualPath"

  fileExists=false

  if [ true = "$isVerbose" ]
  then
    echo "  Found: $file"
  fi

  if [[ true == "$includeCloud" && "$1" == *.icloud ]]
  then
    downloadFile "$file"
  fi

  convertFile "$file" "${2:-0}" # Just Passing Extraction Through for 2nd Param
  if [ true == "$fileExists" ]
  then
    return
  fi

  actualPath "$1"
  file="$actualPath"

  if [[ false == "$keepFiles" || 0 != "${2:-0}" ]]
  then
    if [[ "$file" == *.rar ]]
    then
      file="${file}.old"
    fi

    if [ true = "$isVerbose" ]
    then
      echo "  Removing: $file"
    fi

    rm -f "$file"
  fi

  actualPath "$1"
  file="$actualPath"

  if [[ true == "$evictFiles" && 0 == "${2:-0}" ]] # Only Evict Top Level Iterations
  then
    actualPath "$file"
    if [[ "$actualPath" == *.zip ]]
    then
      actualPath="${actualPath%\.zip}.rar"
    fi

    evictFile "$actualPath"

    if [ true == "$keepFiles" ]
    then
      evictFile "$file"
    fi
  fi
}

scanFiles()
{
  actualPath "$1"
  filePath="$actualPath"

  if [ true = "$isVerbose" ]
  then
    echo "Scanning $filePath"
  fi

  while IFS= read -r -d '' file
  do
    processFile "$file" "${2:-0}"
  done < <(find "$filePath" -name "__MACOSX" -prune -o \( "${findOptions[@]}" \) -print0)
}

convertFile()
{

  actualPath "$1"
  cloudPath "$actualPath"

  zipName="$actualPath"
  rarName="$zipName"

  if [[ "$zipName" == *.zip ]]
  then
    rarName="${zipName%\.zip}.rar"
  fi

  extractPath="${2:-0}"

  cloudPath "$rarName"

  if [ true = "$isVerbose" ]
  then
    echo "Processing: $zipName"
    echo "  Cloud: $cloudPath"
    echo "  Rar: $rarName"
  fi

  if [[ false == "$overwriteExisting" && "$rarName" != "$zipName" ]]
  then
    if [[ -e "$rarName" || -e "$cloudPath" ]]
    then
      fileExists=true
      echo "File Exists"
      return
    fi
  fi

  # Remove Old Extraction If Exists
  rm -rf "$extractionPath/$extractPath"
  mkdir -p "$extractionPath/$extractPath"

  if [[ "$zipName" == *.rar ]]
  then
    mv "$zipName" "${zipName}.old"
    zipName="${zipName}.old"

    unrar x "${zipName}" "$extractionPath/$extractPath/"
  else
    unzip "$zipName" -d "$extractionPath/$extractPath"
  fi

  rm -f "$cloudPath" #JIC
  rm -f "$rarName" #JIC

  if [ true == "$recursiveExtract" ]
  then
    scanFiles "$extractionPath/$extractPath" "$(calc $extractPath + 1)"

    # I'm Lazy And Don't Want To Deal With Recursive Layers Here. Just Reload The Vars.
    actualPath "$1"
    cloudPath "$actualPath"

    zipName="$actualPath"
    rarName="$zipName"

    if [[ "$zipName" == *.zip ]]
    then
      rarName="${zipName%\.zip}.rar"
    fi

    extractPath="${2:-0}"

    cloudPath "$rarName"
  fi

  pushd "$extractionPath/$extractPath"
    find . -type d -name "__MACOSX" -prune -exec rm -rf "{}" \; || exit 0
    find . -type f -name ".DS_Store" -prune -exec rm -rf "{}" \; || exit 0
    find . -type f -name "desktop.ini" -prune -exec rm -rf "{}" \; || exit 0

    rar a "$rarName" ./*
  popd

  rm -rf "$extractionPath/$extractPath"
}

cloudPath()
{
  actualPath="$1"

  fileName=$(basename "$actualPath")
  basePath=$(dirname "$actualPath")

  fileName=".$fileName.icloud"

  cloudPath="$basePath/$fileName"
}

actualPath()
{
    cloudPath="$1"

    fileName=$(basename "$cloudPath")
    basePath=$(dirname "$cloudPath")

    fileName=${fileName#.}
    fileName=${fileName%.icloud}

    actualPath="$basePath/$fileName"
}

evictFile()
{
  actualPath "$1"

  echo "Evicting File: $actualPath"

  # Keep Trying Until It Succeeds
  until brctl evict "$actualPath"
  do
    # Wait To Give Time To Sync
    sleep 10
  done
}

downloadFile()
{
  actualPath "$1"

  echo "Downloading File: $actualPath"

  brctl download "$actualPath"

  # Wait For File To Exist
  while [ ! -e "$actualPath" ]
  do
    # Wait To Give Time To Sync
    sleep 1
  done
}

showHelp()
{
  echo "OS X ReZip Script"
  echo ""
  echo "Recursively scans for any *.zip file and converts them to rar files."
  echo "   Note: Requires Free space equal to the size of the file being extracted."
  echo "   Note: Files will be extracted to '/tmp'."
  echo "   Note: Use at your own risk."
  echo ""
  echo "Syntax: ./rezip.sh [-h|d|v|i|o|e|r|u|p path]"
  echo "options: "
  echo "h    Print the help"
  echo "d    Delete Original Zip Files"
  echo "v    Verbose Mode"
  echo "p    Path to Scan (defaults to current directory)"
  echo "i    Include iCloud Files"
  echo "x    Enable Debug Mode"
  echo "o    Overwrite existing file"
  echo "e    Evict File Once Uploaded"
  echo "r    Recursively Extract Zips Within Zips"
  echo "u    Include Existing RARs In Scan (Recompressing / Removing Zips In RARs, Requires unrar)"
  echo ""
}

# Build Options
while getopts "ixhdvoerup:" option; do
  case $option in
    h) # Display Help
      showHelp
      exit;;
    d) # Delete Original Zip
      keepFiles=false
      ;;
    p) # Set Path
      scanPath="${OPTARG}"
      ;;
    v) # Verbose Mode
      isVerbose=true
      ;;
    i) # iCloud
      includeCloud=true
      ;;
    x)
      debugMode=true
      ;;
    o)
      overwriteExisting=true
      ;;
    e)
      evictFiles=true
      ;;
    r)
      recursiveExtract=true
      ;;
    u)
      recompress=true
      ;;
    \?) # Invalid Option
        echo "Error: Invalid Option: $option"
        exit 2
        ;;
  esac
done

if [ true = "$debugMode" ]
then
  set -x
fi

if [ ! -d "$scanPath" ]
then
  echo "Error: Invalid Path: $scanPath"
  exit 1
fi

if [ true = "$isVerbose" ]
then
  echo "Path: $scanPath"
  echo "Keep Files: $keepFiles"
  echo "Include Cloud: $includeCloud"
  echo "Overwrite Existing: $overwriteExisting"
  echo "Recursive Extract: $recursiveExtract"
  echo "Extraction Path: $extractionPath"
  echo "Recompressing: $recompress"
fi

if [ true = "$includeCloud" ]
then
  findOptions=("${findOptions[@]}" -o -name "*.zip.icloud")
fi

if [ true = "$recompress" ]
then
  findOptions=("${findOptions[@]}" -o -name "*.rar")

  if [ true = "$includeCloud" ]
  then
    findOptions=("${findOptions[@]}" -o -name "*.rar.icloud")
  fi
fi

rm -rf /tmp/tf-extract #JIC
scanFiles "$scanPath"