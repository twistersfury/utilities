#!/usr/bin/env bash

set -e

CURRENT_DIRECTORY=$(pwd)

read -p "Extension Name: " ZEPHIR_EXT_NAME
read -p "Namespace: " ZEPHIR_EXT_NAMESPACE
read -p "Description: " ZEPHIR_EXT_DESC
read -p "Author: " ZEPHIR_EXT_AUTHOR
read -p "E-Mail: " ZEPHIR_EXT_EMAIL

ZEPHIR_EXT_NAME=$(echo "$ZEPHIR_EXT_NAME" | tr '[:upper:]' '[:lower:]')
ZEPHIR_EXT_NAMESPACE_NORMALIZED=$(echo "$ZEPHIR_EXT_NAMESPACE\\" | tr '[:upper:]' '[:lower:]' | cut -d '\' -f 1)
ZEPHIR_EXT_PATH=$(echo "$ZEPHIR_EXT_NAMESPACE\\" | tr '[:upper:]' '[:lower:]' | tr '\' '/')

echo "Extension Name   : ${ZEPHIR_EXT_NAME}"
echo "Namespace        : ${ZEPHIR_EXT_NAMESPACE}"
echo "Namespace Alt    : ${ZEPHIR_EXT_NAMESPACE_NORMALIZED}"
echo "Path             : ${ZEPHIR_EXT_PATH}"
echo "Namespace Escaped: ${ZEPHIR_EXT_NAMESPACE//\\/\\\\}"
echo "Description      : ${ZEPHIR_EXT_DESC}"
echo "Author           : ${ZEPHIR_EXT_AUTHOR}"
echo "E-Mail           : ${ZEPHIR_EXT_EMAIL}"

ZEPHIR_EXT_NAMESPACE=$(builtin printf '%q' "${ZEPHIR_EXT_NAMESPACE}")

find . -type f -not -name init-template.sh -a -not -path "./.*" -a -not -path "./bin*" -a -not -path "./vendor/*" -print0 | while read -d $'\0' file
do
    echo "$file"

    sed -i '' "s#extension_name#${ZEPHIR_EXT_NAME}#g" "$file"
    sed -i '' "s#extension__namespace#${ZEPHIR_EXT_NAMESPACE}#g" "$file"
    sed -i '' "s#extension___namespace#${ZEPHIR_EXT_NAMESPACE_NORMALIZED}#g" "$file"
    sed -i '' "s#extension____namespace#${ZEPHIR_EXT_NAMESPACE//\\/\\\\}#g" "$file"
    sed -i '' "s#extension_description#${ZEPHIR_EXT_DESC}#g" "$file"
    sed -i '' "s#extension_author#${ZEPHIR_EXT_AUTHOR}#g" "$file"
    sed -i '' "s#extension_email#${ZEPHIR_EXT_EMAIL}#g" "$file"
    sed -i '' "s#extension_path#${ZEPHIR_EXT_PATH}#g" "$file"
done

# Adjusting Build Script
sed -i '' 's#extension_name#${ZEPHIR_EXT_NAME}#g' "$CURRENT_DIRECTORY/bin/zephir-build.sh"

POS=0

pushd .

for folder in ${ZEPHIR_EXT_NAMESPACE//\\/ }
do
    echo "Folder: $folder"

    POS=$((POS+1))

    if [ $POS -eq 1 ]; then
        folder=$(echo "$folder" | tr '[:upper:]' '[:lower:]')
    fi

    mkdir -p $folder

    cd $folder
done

mv "${CURRENT_DIRECTORY}/extension___namespace/Kernel.zep" .
mv "${CURRENT_DIRECTORY}/extension___namespace/Di" .

popd

cd "$CURRENT_DIRECTORY"

echo

rmdir extension___namespace
