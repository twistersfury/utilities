#!/bin/bash

###########################################################################
# OS X Ping Scrit
###########################################################################
# Reference: https://www.linuxquestions.org/questions/linux-software-2/ping-shell-script-to-report-high-rtt-768162/
###########################################################################
# Monitors The Ping Rate Against The Given IP Address. Will Automatically
# Notify User That Ping Rate Is High Via Speakers
###########################################################################
# Usage: ./ping.sh <IP> <Ping> <Time>
#   IP: IP Address To Ping
#   Ping: Max Allowed Ping
#   Time: How Long To Wait Between Pings
###########################################################################
# Example: ./ping 8.8.8.8 400 5
###########################################################################

IP=$1 #Destination address
MAX=$2 #Max time in milisecons to record
SEG=$3 #number of seconds for each ping

ping="$(which ping)"

###########################################################
# FUNCTIONS
#
function float_cond()
{
	local cond=0
	if [[ $# -gt 0 ]]; then
		cond=$(echo "$*" | bc -q 2>/dev/null)
		if [[ -z "$cond" ]]; then cond=0; fi
		if [[ "$cond" != 0  &&  "$cond" != 1 ]]; then cond=0; fi
	fi
	local stat=$((cond == 0))
	return $stat
}
###########################################################


while true
do

	RESULT=`${ping} -c 1 ${IP}`
	RET=`echo "$RESULT" | grep 'time=' | awk '{print $7}' | cut -d '=' -f 2`
	RET2=`echo "$RESULT" | grep 'Request timeout'`
	if float_cond "${RET} > ${MAX}"; then
		echo "["`date '+%T'`"]: reply from ${IP} time=${RET} ms"
		say "High Ping Rate"
		#echo "["`date '+%Y%m%d%k%M%S'`"]  TIME: ${RET}"
	elif [ -n "$RET2" ]; then
		echo "["`date '+%T'`"]: reply from ${IP} Request Timed Out"
		say "Request Timed Out"
	fi
	sleep ${SEG}
done