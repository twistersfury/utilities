#!/bin/bash

###########################################################################
# Budget Script For Calculating Contract Amounts
###########################################################################
# Takes the given amount argument and splits it for easy budgeting for
# Taxes, Tithe, and Fun Money
###########################################################################
# Usage: ./budget.sh amount [-t=0.25] [-i=0.1] [-f=0.1]
#   Amount: Gross Amount Paid
#   [T]axes: The taxes to extract (off gross)
#   T[i]the: The tithe to extract (off gross)
#   [F]un: The fun money amount to extract (off net)
###########################################################################
# Example: ./budget.sh 270.00
###########################################################################
# Methods

showHelp()
{
  echo "Budget Script For Calculating Contract Amounts"
  echo ""
  echo "Takes the given amount argument and splits it for easy budgeting for"
  echo "Taxes, Tithe, and Fun Money"
  echo ""
  echo "Syntax: ./budget.sh amount -[h|t 25|i 10|f 10]"
  echo "options: "
  echo "h     Print the help"
  echo "taxes Percent to apply for taxes."
  echo "tithe Percent to apply for tithe."
  echo "fun   Percent to apply for 'fun money'"
  echo ""
}

calc() { awk "BEGIN{print $*}"; }

set -e

# Defaults

grossAmount=$1
taxes=25
tithe=10
fun=10

# Build Options
while getopts "htif:" option; do
  case $option in
    h) # Display Help
      showHelp
      exit;;
    t) # Taxes Amount
      taxes=${OPTARG}
      ;;
    i) # Tithe Amount
      tithe=${OPTARG}
      ;;
    f) # Fun Money
      fun=${OPTARG}
      ;;
    \?) # Invalid Option
        echo "Error: Invalid Option: $option"
        exit 2
        ;;
  esac
done

totalTaxes=$(calc "$grossAmount * ($taxes / 100)")
totalTithe=$(calc "$grossAmount * ($tithe / 100)")
netAmount=$(calc "$grossAmount - $totalTaxes - $totalTithe")
funMoney=$(calc "$netAmount * ($fun / 100)")
budgetAmount=$(calc "$netAmount - $funMoney")

echo "Taxes: $taxes%"
echo "Tithe: $tithe%"
echo "Fun Money: $fun%"
echo ""
echo "Gross Amount: $grossAmount"
echo "Taxes: $totalTaxes"
echo "Tithe: $totalTithe"
echo "Net Amount: $netAmount"
echo "Fun Money: $funMoney"
echo "Budget Amount: $budgetAmount"