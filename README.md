# Utilities

Various utilities to make life easier.


## Makefiles

Read the [README.md](https://gitlab.com/twistersfury/utilities/-/blob/master/make/README.md)

## OSX Ping

Utility to monitor internet for high ping rates.

```
wget -q -O - https://gitlab.com/twistersfury/utilities/-/raw/master/scripts/osx-ping.sh | bash -s ip [max ping] [frequency]
wget -q -O - https://gitlab.com/twistersfury/utilities/-/raw/master/scripts/osx-ping.sh | bash -s 8.8.8.8 10 1
```

## OSX ReZip

Utility To Convert All .zip Files to .rar.

```
wget -q -O - https://gitlab.com/twistersfury/utilities/-/raw/master/scripts/osx-rezip.sh | bash -s -h
```