######################################################
# Copyright (C) 2023 Twister's Fury.
# Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
######################################################
# This Makefile is still under development. It could
#   change at any time.
######################################################
# Core Configuration
######################################################
BIN_PATH ?= bin
MAKEFILE_PHP := true
MAKEFILE_JUSTNAME ?= $(firstword $(MAKEFILE_LIST))
MAKEFILE_COMPLETE ?= $(CURDIR)/$(MAKEFILE_JUSTNAME)
MAKE_OPTIONS ?= -f $(MAKEFILE_JUSTNAME)

######################################################
# Include Docker
######################################################
ifndef MAKEFILE_DOCKER
include $(BIN_PATH)/docker.mk

$(BIN_PATH)/docker.mk:
	$(MAKEFILE_DOWNLOAD_COMMAND) https://gitlab.com/twistersfury/utilities/raw/v2.0.0/make/docker.mk
endif

######################################################
# Include Local Overrides
######################################################
# If you don't have wget, then you can manually edit
# 	bin/overrides.mk to enable curl

$(BIN_PATH):
	mkdir -p $@

$(BIN_PATH)/overrides.mk: | $(BIN_PATH)
	echo 'MAKEFILE_OVERRIDES := true' > $@
	echo 'MAKEFILE_DOWNLOAD_COMMAND = wget -O $$@' >> $@
	echo '#MAKEFILE_DOWNLOAD_COMMAND = curl -o $$@' >> $@
	echo "Override File Created, You Will Have To Rerun The Previous Command."
	echo "By default, this script attempts to use wget. If you don't have wget, you can use curl instead."
	echo "If you want to use curl, edit the $@ file to use the curl version of MAKEFILE_DOWNLOAD_COMMAND." && exit 1

# Only Include If Not Already Included (Will Cause Errors Otherwise)
ifndef MAKEFILE_OVERRIDES
	include $(BIN_PATH)/overrides.mk
endif

######################################################
# Default Configurations
######################################################

PHP_IMAGE ?= twistersfury/zephir:phalcon
PHP_ARGS ?=
PHP_FILE ?=
PHP_FILE_ARGS ?=
PHP_DOCKER_INTERACTIVE ?= -it
PHP_VOLUMES ?=-v "$(CURDIR)":$(APP_DIRECTORY)
PHP_SERVICE ?= php

DEBUG_ENABLE ?= false
DEBUG_HOST ?= host.docker.internal
DEBUG_VERSION ?= 3

APP_DIRECTORY ?= /app

PHP_SERVER_NAME ?= web.test

ifneq ($(DEBUG_ENABLE),false)
	ifneq ($(DEBUG_VERSION),3)
		PHP_DEBUG_OPTIONS ?= -d xdebug.remote_enable=1 -d xdebug.remote_autostart=1 -d xdebug.remote_host=$(DEBUG_HOST) -d zend_extension=xdebug.so
	else
		PHP_DEBUG_OPTIONS ?= -d zend_extension=xdebug.so -d xdebug.mode="debug" -d xdebug.start_with_request=yes -d xdebug.client_host=$(DEBUG_HOST) -d xdebug.idekey=PHPSTORM -d xdebug.discover_client_host=true
	endif
endif

######################################################
# Core Commands
######################################################
.PHONY: php
php: DOCKER_IMAGE = $(PHP_IMAGE)
php: DOCKER_COMMAND=run --rm $(PHP_DOCKER_INTERACTIVE) $(PHP_VOLUMES) $(PHP_EXTRA_VOLUMES) -w "/$(APP_DIRECTORY)"
php: DOCKER_ARGS=php -d memory_limit=-1 $(PHP_DEBUG_OPTIONS) $(PHP_OPTIONS) $(PHP_FILE) $(PHP_FILE_ARGS)
php: DOCKER_ENVS ?=--env PHP_IDE_CONFIG="serverName=$(PHP_SERVER_NAME)" ${PHP_ENVS}
php: $(PHP_FILE) docker-pull-image docker

.PHONY: php-compose
php-compose: DOCKER_IMAGE = $(PHP_IMAGE)
php-compose: DOCKER_COMPOSE_COMMAND=run
php-compose: DOCKER_COMPOSE_ARGS=php php -d memory_limit=-1 $(PHP_DEBUG_OPTIONS) $(PHP_OPTIONS) $(PHP_FILE) $(PHP_FILE_ARGS)
php-compose: DOCKER_ENVS ?=--env PHP_IDE_CONFIG="serverName=$(PHP_SERVER_NAME)" ${PHP_ENVS} $(PHP_VOLUMES) -w "/$(APP_DIRECTORY)" $(PHP_SERVICE)
php-compose: $(PHP_FILE) docker-pull-image docker-compose
