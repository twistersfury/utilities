######################################################
# Copyright (C) 2023 Twister's Fury.
# Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
######################################################
# This Makefile is still under development. It could
#   change at any time.
######################################################
#                Docker Configuration                #
######################################################
MAKEFILE_DOCKER := true
MAKEFILE_JUSTNAME ?= $(firstword $(MAKEFILE_LIST))
MAKEFILE_COMPLETE ?= $(CURDIR)/$(MAKEFILE_JUSTNAME)
MAKE_OPTIONS ?= -f $(MAKEFILE_JUSTNAME)

######################################################
# Default Configurations
######################################################

DOCKER_COMPOSE_CONFIG ?= -f docker-compose.yml
DOCKER_NETWORK ?= none
DOCKER_BUILD_PULL ?= --pull

# Check For File Existence
ifneq ("$(wildcard docker-compose.override.yml)","")
	DOCKER_COMPOSE_OVERRIDE ?= docker-compose.override.yml
endif

ifneq ("$(DOCKER_COMPOSE_OVERRIDE)","")
	DOCKER_COMPOSE_CONFIG_OVERRIDE ?= -f $(DOCKER_COMPOSE_OVERRIDE)
endif

ifneq ("$(DOCKER_NETWORK)","none")
	DOCKER_NETWORK_ARGS ?= --network "$(DOCKER_NETWORK)"
endif

DOCKER_HUB ?=
DOCKER_COMPOSE_OPTIONS ?= $(DOCKER_COMPOSE_CONFIG) $(DOCKER_COMPOSE_CONFIG_OVERRIDE)

######################################################
#                 Docker Core Commands               #
######################################################

.PHONY: docker
docker: $(BIN_PATH)/.docker
	$(GLOBAL_ENV) docker $(DOCKER_COMMAND) $(DOCKER_NETWORK_ARGS) $(DOCKER_ENVS) $(DOCKER_VOLUMES) $(DOCKER_OPTIONS) $(DOCKER_IMAGE) $(DOCKER_ARGS)

# Start Docker Images - Shortcut
.PHONY: docker-start
docker-start:
	make $(MAKE_OPTIONS) docker-check
	make $(MAKE_OPTIONS) $(BIN_PATH)/.compose

# Stop Docker Images
.PHONY: docker-stop
docker-stop: $(BIN_PATH)/.docker
	make $(MAKE_OPTIONS) docker-compose DOCKER_COMPOSE_COMMAND=down || (echo 'Compose Not Running' && exit 0)
	docker stop $(shell docker ps -q) || (echo 'No Images Running' && exit 0)
	rm -f $(BIN_PATH)/.compose

.PHONY: docker-build
docker-build:
	rm -f $(BIN_PATH)/.build
	make $(MAKE_OPTIONS) $(BIN_PATH)/.build DOCKER_BUILD_ARGS="$(DOCKER_BUILD_ARGS)" DOCKER_OPTIONS="$(DOCKER_OPTIONS)"

.PHONY: docker-pull
docker-pull:
	rm -f $(BIN_PATH)/.pull
	make $(MAKE_OPTIONS) $(BIN_PATH)/.pull

# Stop & Remove
.PHONY: docker-clean
docker-clean: DOCKER_COMPOSE_ARGS="-v"
docker-clean: docker-stop

.PHONY: docker-check
docker-check:
	docker ps -q --no-trunc | grep $$(cat $(BIN_PATH)/.compose) 2> /dev/null || (echo 'Not Running' && rm -f $(BIN_PATH)/.compose)

.PHONY:
docker-login: $(BIN_PATH)/.docker-login

.PHONY: docker-compose
docker-compose: $(BIN_PATH)/.docker $(BIN_PATH)/.docker-login docker-compose.yml $(DOCKER_COMPOSE_OVERRIDE)
	docker-compose $(DOCKER_COMPOSE_OPTIONS) $(DOCKER_COMPOSE_COMMAND) $(DOCKER_COMPOSE_SERVICE) $(DOCKER_COMPOSE_ARGS)

.PHONY: docker-pull-image
docker-pull-image:
	docker pull $(DOCKER_IMAGE) || (echo "No Image To Pull" && exit 0)

######################################################
#               Docker Helper Commands               #
######################################################
.PHONY: docker-restart
docker-restart: docker-stop docker-start

.PHONY: docker-rebuild
docker-rebuild:
	rm $(BIN_PATH)/.build
	make $(MAKE_OPTIONS) $(BIN_PATH)/.build

.PHONY: docker-push
docker-push: DOCKER_COMPOSE_COMMAND=push
docker-push: docker-compose

.PHONY: docker-ps
docker-ps: DOCKER_COMPOSE_COMMAND=ps
docker-ps: docker-compose

.PHONY: docker-logs
docker-logs: DOCKER_COMPOSE_COMMAND=logs
docker-logs: docker-compose

######################################################
#            Docker Hard File Requirements           #
######################################################

# Local Selenium (For Testing)
$(BIN_PATH)/.compose: $(BIN_PATH)/.docker $(BIN_PATH)/.pull $(BIN_PATH)/.build docker-compose.yml $(DOCKER_COMPOSE_OVERRIDE) | $(BIN_PATH)
	# Stop Any Running Docker Items
	make $(MAKE_OPTIONS) docker-stop
	make $(MAKE_OPTIONS) docker-compose DOCKER_COMPOSE_COMMAND="up -d"
	# More or less a dummy placeholder - Just an image that isn't used that doesn't exit for CID File.
	docker run --cidfile $(BIN_PATH)/.compose --rm -d nginx
	# Sleep To Allow Everything Time To Boot
	sleep 20

# Docker Installation
$(BIN_PATH)/.docker: | $(BIN_PATH)
	which docker || (echo "Docker Is Not Installed" && exit 1)
	which docker-compose || (echo "Docker Compose Is Not Installed" && exit 1)
	touch $@

$(BIN_PATH)/.pull:
	make docker-compose DOCKER_COMPOSE_COMMAND=pull
	touch $@

$(BIN_PATH)/.build:
	make docker-compose DOCKER_COMPOSE_COMMAND="build $(DOCKER_BUILD_PULL) $(DOCKER_BUILD_ARGS) $(DOCKER_OPTIONS)"
	touch $(BIN_PATH)/.build
$(BIN_PATH)/.docker-login: | $(BIN_PATH)
	docker login $(DOCKER_LOGIN_OPTIONS) $(DOCKER_HUB)
	touch $@
