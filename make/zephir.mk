######################################################
# Copyright (C) 2023 Twister's Fury.
# Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
######################################################
# This Makefile is still under development. It could
#   change at any time.
######################################################
# This Makefile makes the following assumptions:
#   1. There is a defined variable $(BIN_PATH)
#   2. There is a defined variable $(ZEPHIR_EXT_NAME)
#   3. There is a folder named $(BIN_PATH)
#   4. There is a docker-compose.yml file
#   5. There is a service named "$(ZEPHIR_SERVICE)" (php
#      by default) that uses a 'build' index.
#   6. The Dockerfile for the build contains the lines
#        ENV ZEPHIR_EXT_NAME $(ZEPHIR_EXT_NAME))
#        COPY $(BIN_DIR) (path to workdir)
#        RUN $(BIN_DIR)/zephir-build.sh
######################################################
# Core Configuration
######################################################
BIN_PATH ?= bin
MAKEFILE_ZEPHIR := true
MAKEFILE_JUSTNAME ?= $(firstword $(MAKEFILE_LIST))
MAKEFILE_COMPLETE ?= $(CURDIR)/$(MAKEFILE_JUSTNAME)
MAKE_OPTIONS ?= -f $(MAKEFILE_JUSTNAME)

######################################################
# Include Docker
######################################################
ifndef MAKEFILE_DOCKER

include $(BIN_PATH)/docker.mk

$(BIN_PATH)/docker.mk:
	$(MAKEFILE_DOWNLOAD_COMMAND) https://gitlab.com/twistersfury/utilities/raw/v2.0.0/make/docker.mk
endif

######################################################
# Include Composer
######################################################
ifndef MAKEFILE_COMPOSER

include $(BIN_PATH)/codecept.mk

$(BIN_PATH)/composer.mk: | $(BIN_PATH)
	$(MAKEFILE_DOWNLOAD_COMMAND) https://gitlab.com/twistersfury/utilities/raw/v2.0.0/make/composer.mk
endif

######################################################
# Include Local Overrides
######################################################
# If you don't have wget, then you can manually edit
# 	bin/overrides.mk to enable curl

$(BIN_PATH):
	mkdir -p $@

$(BIN_PATH)/overrides.mk: | $(BIN_PATH)
	echo 'MAKEFILE_OVERRIDES := true' > $@
	echo 'MAKEFILE_DOWNLOAD_COMMAND = wget -O $$@' >> $@
	echo '#MAKEFILE_DOWNLOAD_COMMAND = curl -o $$@' >> $@
	echo "Override File Created, You Will Have To Rerun The Previous Command."
	echo "By default, this script attempts to use wget. If you don't have wget, you can use curl instead."
	echo "If you want to use curl, edit the $@ file to use the curl version of MAKEFILE_DOWNLOAD_COMMAND." && exit 1

# Only Include If Not Already Included (Will Cause Errors Otherwise)
ifndef MAKEFILE_OVERRIDES
	include $(BIN_PATH)/overrides.mk
endif

######################################################
# Default Configurations
######################################################

PHP_IMAGE ?= twistersfury/zephir:v0.15.2
DOCKER_IMAGE ?= $(PHP_IMAGE)

APP_DIRECTORY ?= /app
ZEPHIR_SERVICE ?= php
ZEPHIR_PATH ?= /composer/vendor/bin/zephir

ZEPHIR_BUILD_SCRIPT ?= $(BIN_PATH)/zephir-build.sh
PHP_EXT_VERSION ?= 20190902

ZEPHIR_CODECEPT_OVERRIDES = "yes"
ZEPHIR_BUILD_IMAGE ?= twistersfury/zephir:v0.15.2

ifeq ("$(ZEPHIR_CODECEPT_OVERRIDES)","yes")

CODECEPT_IMAGE ?= $(ZEPHIR_BUILD_IMAGE)

endif

######################################################
# Hard Dependencies
######################################################
$(BIN_PATH)/zephir-build.sh:
	$(MAKEFILE_DOWNLOAD_COMMAND) https://gitlab.com/twistersfury/utilities/-/raw/v1.0.11/scripts/zephir/zephir-build.sh
	chmod +x $@

$(BIN_PATH)/init-template.sh:
	$(MAKEFILE_DOWNLOAD_COMMAND) https://gitlab.com/twistersfury/utilities/raw/master/scripts/zephir/init-template.sh
	chmod +x $@

$(BIN_PATH)/.init-template: $(BIN_PATH)/zephir-build.sh $(BIN_PATH)/init-template.sh
	$(BIN_PATH)/init-template.sh
	touch $@

$(BIN_PATH)/.zephir-test:
	rm -f $(BIN_PATH)/.zephir-*
	$(MAKE) zephir-build-coverage

ext/modules/$(ZEPHIR_EXT_NAME).so: $(shell find . -type f -name "*.zep")
	$(MAKE) zephir-compile DOCKER_ENVS=$(DOCKER_ENVS) || exit 1
	touch $@

######################################################
# Core Commands
######################################################

.PHONY: zephir
zephir: DOCKER_NETWORK_ARGS=
zephir: PHP_FILE_ARGS=$(ZEPHIR_COMMAND) $(ZEPHIR_OPTIONS)
zephir: PHP_FILE=$(ZEPHIR_PATH)
zephir: PHP_ENVS=--env ZEPHIR_EXT_NAME=${ZEPHIR_EXT_NAME}
zephir: php

.PHONY: zephir-build-restart
zephir-build-restart: $(BIN_PATH)/zephir-build.sh docker-build docker-restart

.PHONY: zephir-clean-build
zephir-clean-build: zephir-clean zephir-full-clean zephir-build

.PHONY: zephir-clean
zephir-clean: ZEPHIR_COMMAND=clean
zephir-clean: zephir

.PHONY: zephir-full-clean
zephir-full-clean: ZEPHIR_COMMAND=fullclean
zephir-full-clean: zephir

.PHONY: zephir-build
zephir-build: $(ZEPHIR_BUILD_SCRIPT) docker-build

.PHONY: zephir-generate
zephir-generate: DOCKER_EXTRA_OPTIONS=--env ZEPHIR_DEBUG=1
zephir-generate: ZEPHIR_COMMAND="generate"

.PHONY: zephir-compile
zephir-compile: DOCKER_IMAGE=$(ZEPHIR_BUILD_IMAGE)
zephir-compile: DOCKER_VOLUMES=-v "$(CURDIR)":$(APP_DIRECTORY) $(ZEPHIR_DEBUG)
zephir-compile: DOCKER_ARGS=$(ZEPHIR_BUILD_SCRIPT)
zephir-compile: DOCKER_COMMAND=run --rm -it --env ZEPHIR_EXT_NAME="$(ZEPHIR_EXT_NAME)"
zephir-compile: DOCKER_NETWORK=none
zephir-compile: docker

.PHONY: zephir-compile-debug
zephir-compile-debug: ZEPHIR_DEBUG=--env REPORT_COVERAGE=1
zephir-compile-debug: zephir-compile

.PHONY: zephir-stubs
zephir-stubs: ZEPHIR_COMMAND=stub
zephir-stubs: zephir

.PHONY: zephir-generate-coverage
zephir-generate-coverage: DOCKER_IMAGE=$(ZEPHIR_BUILD_IMAGE)
zephir-generate-coverage:
	set -e; $(DOCKER_COMMAND) lcov --no-external --capture --quiet --output-file coverage.info --directory .
	set -e; $(DOCKER_COMMAND) genhtml -q -o report coverage.info

.PHONY: init-template
init-template: $(BIN_PATH)/.init-template
	set -e; $(MAKE) install
	set -e; $(MAKE) zephir-build-restart
	set -e; $(MAKE) test

.PHONY: zephir-build-coverage
zephir-build-coverage: PHP_ENVS=--env REPORT_COVERAGE=1
zephir-build-coverage: zephir-clean zephir-full-clean zephir-build

######################################################
# Codecept Commands
######################################################

ifeq ("$(ZEPHIR_CODECEPT_OVERRIDES)","yes")

.PHONY: test
test: DOCKER_VOLUMES=--volume "$(CURDIR)/ext/modules/$(ZEPHIR_EXT_NAME).so":/usr/local/lib/php/extensions/no-debug-non-zts-$(PHP_EXT_VERSION)/$(ZEPHIR_EXT_NAME).so
test: PHP_OPTIONS=-d extension=/app/ext/modules/$(ZEPHIR_EXT_NAME).so
test: ext/modules/$(ZEPHIR_EXT_NAME).so

.PHONY: zephir-test-coverage
zephir-test-coverage: DOCKER_VOLUMES=--volume "$(CURDIR)/ext/modules/$(ZEPHIR_EXT_NAME).so":/usr/local/lib/php/extensions/no-debug-non-zts-$(PHP_EXT_VERSION)/$(ZEPHIR_EXT_NAME).so
zephir-test-coverage: DOCKER_ENVS="--env REPORT_COVERAGE=1 --env ZEPHIR_EXT_NAME=${ZEPHIR_EXT_NAME} --env ZEPHIR_DEBUG=1 --env ZEPHIR_RELEASE=0 --env REPORT_COVERAGE=1"
zephir-test-coverage: PHP_OPTIONS=-d extension=/app/ext/modules/$(ZEPHIR_EXT_NAME).so
zephir-test-coverage: ext/modules/$(ZEPHIR_EXT_NAME).so
	$(MAKE) -e codecept-run CODECEPT_ARGS="--xml=junit.xml --coverage-xml=coverage.xml --coverage-html --coverage-phpunit=codeception-coverage-xml/ --ext TwistersFury\\\\Shared\\\\Tests\\\\Support\\\\Extension\\\\ZephirCoverage $(CODECEPT_ARGS)" PHP_VOLUMES="$(PHP_VOLUMES)" CODECEPT_IMAGE=$(CODECEPT_IMAGE) DOCKER_VOLUMES="$(DOCKER_VOLUMES)" PHP_OPTIONS="$(PHP_OPTIONS)"
	#$(MAKE) zephir-generate-coverage

endif