######################################################
# Copyright (C) 2023 Twister's Fury.
# Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
######################################################
# This Makefile is still under development. It could
#   change at any time.
######################################################
# Core Configuration
######################################################
BIN_PATH ?= bin
MAKEFILE_TERRAFORM := true
MAKEFILE_JUSTNAME ?= $(firstword $(MAKEFILE_LIST))
MAKEFILE_COMPLETE ?= $(CURDIR)/$(MAKEFILE_JUSTNAME)
MAKE_OPTIONS ?= -f $(MAKEFILE_JUSTNAME)

######################################################
# Include SSH
######################################################
ifndef MAKEFILE_SSH
include $(BIN_PATH)/ssh.mk

$(BIN_PATH)/ssh.mk: | $(BIN_PATH)
	$(MAKEFILE_DOWNLOAD_COMMAND) https://gitlab.com/twistersfury/utilities/-/raw/v2.0.0/make/ssh.mk
endif

######################################################
# Include Local Overrides
######################################################
# If you don't have wget, then you can manually edit
# 	bin/overrides.mk to enable curl

$(BIN_PATH):
	mkdir -p $@

$(BIN_PATH)/overrides.mk: | $(BIN_PATH)
	echo 'MAKEFILE_OVERRIDES := true' > $@
	echo 'MAKEFILE_DOWNLOAD_COMMAND = wget -O $$@' >> $@
	echo '#MAKEFILE_DOWNLOAD_COMMAND = curl -o $$@' >> $@
	echo "Override File Created, You Will Have To Rerun The Previous Command."
	echo "By default, this script attempts to use wget. If you don't have wget, you can use curl instead."
	echo "If you want to use curl, edit the $@ file to use the curl version of MAKEFILE_DOWNLOAD_COMMAND." && exit 1

# Only Include If Not Already Included (Will Cause Errors Otherwise)
ifndef MAKEFILE_OVERRIDES
	include $(BIN_PATH)/overrides.mk
endif

######################################################
# Default Configurations
######################################################

TERRAFORM_DIRECTORY ?= ./
TERRAFORM_DEBUG ?= false
TERRAFORM_WORKSPACE ?= terraform

ifeq ($(TERRAFORM_AUTO),true)
	TERRAFORM_AUTO_OPTIONS ?= -auto-approve
endif

ifneq ($(TERRAFORM_DEBUG), false)
	TERRAFORM_DEBUG_ARG := TF_LOG=TRACE
endif


######################################################
# Terraform Commands
######################################################

.PHONY: terraform-rebuild
terraform-rebuild: terraform-destroy terraform-apply

.PHONY: terraform-destroy
terraform-destroy: $(TERRAFORM_DIRECTORY)/$(BIN_PATH)/$(TERRAFORM_WORKSPACE).terraform.workspace $(TERRAFORM_DIRECTORY)/$(TERRAFORM_WORKSPACE).tfvars | $(BIN_PATH)/.ssh-agent
	$(TERRAFORM_DEBUG_ARG) terraform -chdir=$(TERRAFORM_DIRECTORY) destroy -var-file=$$(terraform workspace show).tfvars $(TERRAFORM_OPTIONS) $(TERRAFORM_AUTO_OPTIONS) || exit 0
	rm -f $(BIN_PATH)/.ssh-agent

.PHONY: terraform-apply
terraform-apply: $(TERRAFORM_DIRECTORY)/$(BIN_PATH)/$(TERRAFORM_WORKSPACE).terraform.workspace $(TERRAFORM_DIRECTORY)/$(TERRAFORM_WORKSPACE).tfvars | $(BIN_PATH)/.ssh-agent
	$(TERRAFORM_DEBUG_ARG) terraform -chdir=$(TERRAFORM_DIRECTORY) apply -var-file=$$(terraform workspace show).tfvars $(TERRAFORM_OPTIONS) $(TERRAFORM_AUTO_OPTIONS)

######################################################
# Hard Files
######################################################

$(TERRAFORM_DIRECTORY)/$(BIN_PATH)/.terraform: $(shell find $(TERRAFORM_DIRECTORY) -type f -name "*.tf")
	mkdir -p $(TERRAFORM_DIRECTORY)/$(BIN_PATH)
	$(TERRAFORM_DEBUG_ARG) terraform -chdir=$(TERRAFORM_DIRECTORY) init
	touch $@

$(TERRAFORM_DIRECTORY)/$(BIN_PATH)/$(TERRAFORM_WORKSPACE).terraform.workspace: | $(TERRAFORM_DIRECTORY)/$(BIN_PATH)/.terraform
	rm -f $(BIN_PATH)/*.terraform.workspace
	terraform workspace new $(TERRAFORM_WORKSPACE) || exit 0
	terraform workspace select $(TERRAFORM_WORKSPACE) || exit 0
	touch $@