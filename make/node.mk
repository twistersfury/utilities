######################################################
# Copyright (C) 2023 Twister's Fury.
# Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
######################################################
# This Makefile is still under development. It could
#   change at any time.
######################################################
# This Makefile makes the following assumptions:
#   1. There is a defined variable $(BIN_PATH)
#   2. There is a defined variable $(ZEPHIR_EXT_NAME)
#   3. There is a folder named $(BIN_PATH)
#   4. There is a docker-compose.yml file
#   5. There is a service named "$(ZEPHIR_SERVICE)" (php
#      by default) that uses a 'build' index.
#   6. The Dockerfile for the build contains the lines
#        ENV ZEPHIR_EXT_NAME $(ZEPHIR_EXT_NAME))
#        COPY $(BIN_DIR) (path to workdir)
#        RUN $(BIN_DIR)/zephir-build.sh
######################################################
# Core Configuration
######################################################
MAKEFILE_NODE := true
MAKEFILE_JUSTNAME ?= $(firstword $(MAKEFILE_LIST))
MAKEFILE_COMPLETE ?= $(CURDIR)/$(MAKEFILE_JUSTNAME)
MAKE_OPTIONS ?= -f $(MAKEFILE_JUSTNAME)

######################################################
# Include Docker
######################################################
$(BIN_PATH)/docker.mk: Makefile | $(BIN_PATH)
	$(MAKEFILE_DOWNLOAD_COMMAND) https://gitlab.com/twistersfury/utilities/-/raw/v2.3.0/make/docker.mk

ifndef MAKEFILE_DOCKER
include $(BIN_PATH)/docker.mk
endif


######################################################
# Default Configurations
######################################################

NODE_DOCKER_IMAGE ?= node
NODE_ARGS ?= yarn install
NODE_PORT ?= 3000

CYPRESS_VOLUME ?= cypress

######################################################
# Default Targets
######################################################

.PHONY: node
node: DOCKER_IMAGE=$(NODE_DOCKER_IMAGE)
node: DOCKER_COMMAND=run
node: DOCKER_VOLUMES=-v "$(CURDIR)":/app -w /app $(NODE_VOLUMES)
node: DOCKER_ARGS ?= $(NODE_ARGS)
node: DOCKER_NETWORK=default
node: package.json
node: docker

.PHONY: node-build
node-build: NODE_ARGS=yarn run build
node-build: yarn.lock node

.PHONY: node-require
node-require: NODE_ARGS=yarn add $(NODE_PACKAGES)
node-require: node

######################################################
# Script Targets
######################################################

.PHONY: npm-dev
npm-dev: NODE_ARGS=yarn run dev
npm-dev: DOCKER_OPTIONS=-p 3000:3000/tcp
npm-dev: node

######################################################
# TypeScript Targets
######################################################

.PHONY: ts-version
ts-version: NODE_ARGS=node_modules/.bin/tsc  --v
ts-version: node

######################################################
# Cypress Targets
######################################################

.PHONY: test
test: cypress-run

.PHONY: cypress
cypress: NODE_ARGS=yarn run cypress $(CYPRESS_COMMAND)
cypress: NODE_VOLUMES=-v $(CYPRESS_VOLUME):/root/.cache
cypress: DOCKER_OPTIONS=--ipc=host
cypress: NODE_DOCKER_IMAGE=cypress/browsers:node16.16.0-chrome107-ff107-edge
#cypress: DOCKER_ENVS=--env ELECTRON_RUN_AS_NODE=1
cypress: $(BIN_PATH)/cypress node | $(CYPRESS_CACHE_DIR)

.PHONY: cypress-run
cypress-run: CYPRESS_COMMAND=run
cypress-run: cypress

.PHONY: cypress-install
cypress-install: CYPRESS_COMMAND=install
cypress-install: node | $(CYPRESS_CACHE_DIR)

######################################################
# Jest Targets
######################################################

.PHONY: test-unit
test-unit: jest

.PHONY: jest
jest: NODE_ARGS=yarn run jest
jest: node

.PHONY: jest-init
jest-init: NODE_ARGS=yarn run jest --init
jest-init: DOCKER_OPTIONS=-it
jest-init: node

.PHONY: ts-jest-init
ts-jest-init: NODE_ARGS=yarn run ts-jest config:init
ts-jest-init: DOCKER_OPTIONS=-it
ts-jest-init: node

######################################################
# Hard Files
######################################################

$(BIN_PATH)/cypress:
	docker volume create $(CYPRESS_VOLUME)
	make cypress-install
	touch $@

yarn.lock:
	make node