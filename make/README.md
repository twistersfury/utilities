## Makefile Utilities

The files here are intended to be reusable Makefiles. They can be included very easily as indicated below.
 
#### Installation/Usage
Add the below snippet to the top of your Makefile. This is required in order for the subsequent includes to work correctly. You will need to uncomment one of the `MAKEFILE_DOWNLOAD_COMMAND` variables. You of course set it to your flavor, so long as the URL goes at the end of the command to download and the output is sent to `$@`.

```Makefile
######################################################
# Core Configuration
######################################################

BIN_PATH := bin

#MAKEFILE_DOWNLOAD_COMMAND = wget -O $@
#MAKEFILE_DOWNLOAD_COMMAND = curl -o $@
MAKEFILE_JUSTNAME := $(firstword $(MAKEFILE_LIST))
MAKEFILE_COMPLETE := $(CURDIR)/$(MAKEFILE_JUSTNAME)
MAKE_OPTIONS := -f $(MAKEFILE_JUSTNAME)

$(BIN_PATH):
	mkdir -p $@
```

Replace `[Makefile]` with the name of the subfile you'd like to include.

*Note*: The `ifndef` is not required. However, removing it will cause make to generate a lot of warnings.

```Makefile
######################################################
# Include [Makefile]
######################################################
ifndef MAKEFILE_[MAKEFILE]

include $(BIN_PATH)/[Makefile].mk

$(BIN_PATH)/[Makefile].mk:
	mkdir -p $(BIN_PATH)
	$(MAKEFILE_DOWNLOAD_COMMAND) https://gitlab.com/twistersfury/utilities/raw/master/make/[Makefile].mk
endif
```

#### Per-User Overrides

If using these files in a team environment, it is likely some variables may need to be different (IE: User Path). To help accomidate this, all the makefiles will attempt to load `bin/overrides.mk`. All defined variables will use `?=`, allowing them to be defined instead in the override file. One likely scenario would be where different team members may have curl while others have wget. In this case, you would manually include the override in your makefile before the default `MAKEFILE_` commands.

```makefile
BIN_PATH := bin

ifndef MAKEFILE_OVERRIDES
include $(BIN_PATH)/overrides.mk

$(BIN_PATH)/overrides.mk: | $(BIN_PATH)
	touch $@
	echo "MAKEFILE_OVERRIDES := true" >> $@
	echo "MAKEFILE_DOWNLOAD_COMMAND ?= wget -O $@" >> $@
endif

MAKEFILE_DOWNLOAD_COMMAND ?= wget -O $@
```

#### codecept.mk
Codeception related Make commands. These are intended to run codeception against the project in a docker isolated environment.

_Includes:_ Composer, Php

###### Targets

1. `test` - Run all suites.
1. `test-coverage` - Run all suites with coverage enabled.
1. `test-unit` - Run just the unit suites.
1. `test-functional` - Run just the functional suites.
1. `test-acceptance` - Run just the acceptance suites.
1. `test-feature` - Run just the feature suite. *This is intended for gherkin based tests. It isn't a default of codeception.*
1. `test-feature-dry` - Run the feature suite in dry run mode.
1. `codecept-build` - Run the build command
1. `codecept-gherkin` - Run the gherkin:snippets command.

#### composer.mk
Composer related Make commands.

_Includes_: Docker

###### Targets

1. `composer-install` - Runs `composer install --ignore-platform-reqs`
1. `composer-dump-autoload` - Runs `composer update --ignore-platform-reqs`
1. `composer-update-static` - Runs Composer Utility Script to Update all Packages to latest.
1. `composer` - Combined With `COMPOSER_COMMAND` and `COMPOSER_OPTIONS`, can run items like require. IE: `composer COMPOSER_COMMAND=require COMPOSER_OPTIONS=some/package`

#### docker.mk
Adds Docker Related Commands. Other Makefiles commonly use it.

_Includes_: Bin

###### Targets

1. `docker` - Core Command
1. `docker-start` - Runs `docker-compose up`
1. `docker-stop` - Shuts down running Docker Images
1. `docker-build` - Runs `docker-compose build`
1. `docker-pull` - Runs `docker-compose pull`
1. `docker-clean` - Run `docker-compose down -v` to remove volumes.
1. `docker-login` - Run Docker Login
1. `docker-pull-image` - Run Docker Pull on `DOCKER_IMAGE`

#### example.mk

Just an example usage for the raw Makefile.

#### local-overrides.mk

This will mostly be used by all existing makefiles, though you can use it directly. This is specifically for team scenarios where certain variables (like paths) may be different for each team member.

#### php.mk

A makefile specific for running PHP commands in PHP (IE: Codeception or Artisan)

###### Targets

1. `php` - Runs the PHP Command. Use `PHP_OPTIONS`, `PHP_FILE`, and `PHP_FILE_ARGS` to make adjustments. Use `DEBUG_ENABLE` to enable xdebug options in command.

#### phpcs.mk

Simple Makefile for running phpcs using the docker image `cytopia/phpcs` and `cytopia/phpcbf`.

###### Targets
1. `cs-check` - Run Check of Files
1. `cs-fix` - Runs fixes of Files.

#### terraform.mk
Adds Terraform Related Commands

###### Targets
1. `terraform-rebuild` - Destroys & Rebuilds The Environment
1. `terraform-apply` - Runs `terraform apply`
1. `terraform-destroy` - Runs `terraform destroy`

###### Configuration
1. `TERRAFORM_DIRECTORY` - The directory to run terraform in (Relative to Makefile)
1. `TERRAFORM_AUTO` - If set to anything other than `false`, will enable Terraform `-auto-aprove`
1. `TERRAFORM_OPTIONS` - Add additional terraform options.
1. `TERRAFORM_DEBUG` - Will enable trace logging.

###### Example Usage
```Makefile
gitlab-apply: ssh-keys/terraform.tfstate
gitlab-apply: TERRAFORM_DIRECTORY=gitlab
gitlab-apply: terraform-apply
```

###### Notes
1. Commands will run `terraform init` automatically.
1. Changing any `.tf` files will rerun `terraform init`.

#### zephir.mk
Adds Zephir Related Commands

###### Targets
1. `zephir-build` - Builds Zephir Plugin
1. `zephir-generate` - Runs Zephir Generate
1. `zephir-compile` - Runs Zephir Compile
1. `zephir-stubs` - Runs Zephir Stubs
1. `zephir-generate-coverage` - Runs lcov to Generate Coverage
1. `zephir-clean-build` - Runs `zehphir clean` followed by `zephir build`.
1. `zephir-full-clean` - Runs `zephir fullclean`
1. `test` - Compliments Exiting `test` command (IE: Codecept)
1. `test-coverage` - Runs Testing with Coverage Built (Requires Rebuild)

###### Configuration
1. `TERRAFORM_DIRECTORY` - The directory to run terraform in (Relative to Makefile)
1. `TERRAFORM_AUTO` - If set to anything other than `false`, will enable Terraform `-auto-aprove`
1. `TERRAFORM_OPTIONS` - Add additional terraform options.
1. `TERRAFORM_DEBUG` - Will enable trace logging.