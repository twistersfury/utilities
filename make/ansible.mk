######################################################
# Copyright (C) 2023 Twister's Fury.
# Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
######################################################
# This Makefile is still under development. It could
#   change at any time.
######################################################
# This Makefile makes the following assumptions:
#   1. There is a defined variable $(BIN_PATH)
#   2. There is a folder named $(BIN_PATH)
######################################################
#                Helm Configuration                #
######################################################

BIN_PATH ?= bin

MAKEFILE_ANISBLE := true
MAKEFILE_JUSTNAME ?= $(firstword $(MAKEFILE_LIST))
MAKEFILE_COMPLETE ?= $(CURDIR)/$(MAKEFILE_JUSTNAME)
MAKE_OPTIONS ?= -f $(MAKEFILE_JUSTNAME)

######################################################
# Ansible Variables
######################################################

ANSIBLE_CHECK ?= --check
ANSIBLE_DIFF ?= --diff
ANSIBLE_DIRECTORY ?= ansible
ANSIBLE_INVENTORY ?= inventories/production
ANSIBLE_INVENTORY_CONFIG ?= hosts.yaml
ANSIBLE_INVENTORY_VAULT ?= group_vars/all.yaml
ANSIBLE_PLAYBOOK ?= install.yaml
ANSIBLE_VAULT_IDS ?= --vault-id ansible-vault@prompt

######################################################
# Ansible Commands
######################################################

.PHONY: ansible
ansible: ansible-playbook

.PHONY: ansible-playbook
ansible-playbook: $(BIN_PATH)/.ansible
	make $(ANSIBLE_DIRECTORY)/$(ANSIBLE_INVENTORY)/$(ANSIBLE_INVENTORY_CONFIG)
	cd $(ANSIBLE_DIRECTORY) && ansible-playbook $(ANSIBLE_PLAYBOOK) -i $(ANSIBLE_INVENTORY) $(ANSIBLE_DIFF) $(ANSIBLE_CHECK) $(ANSIBLE_VAULT_IDS)

.PHONY: ansible-vault
ansible-vault: $(BIN_PATH)/.ansible
	make ANSIBLE_DIRECTORY=$(ANSIBLE_DIRECTORY) ANSIBLE_INVENTORY=$(ANSIBLE_INVENTORY) ANSIBLE_INVENTORY_VAULT=$(ANSIBLE_INVENTORY_VAULT) $(ANSIBLE_DIRECTORY)/$(ANSIBLE_INVENTORY)/$(ANSIBLE_INVENTORY_VAULT)
	cd $(ANSIBLE_DIRECTORY) && ansible-vault edit $(ANSIBLE_VAULT_IDS) $(ANSIBLE_INVENTORY)/$(ANSIBLE_INVENTORY_VAULT)

######################################################
# Ansible Hard Files
######################################################

$(BIN_PATH)/.ansible:
	which ansible && touch $@

$(ANSIBLE_DIRECTORY)/$(ANSIBLE_INVENTORY):
	@echo ""
	@echo "Invalid Inventory File Provided: $(ANSIBLE_DIRECTORY)/$(ANSIBLE_INVENTORY). Please adjust ANSIBLE_DIRECTORY and/or ANSIBLE_INVENTORY"
	@echo "" && exit 1

$(ANSIBLE_DIRECTORY)/$(ANSIBLE_INVENTORY)/$(ANSIBLE_INVENTORY_VAULT):
	cd $(ANSIBLE_DIRECTORY) && ansible-vault create $(ANSIBLE_VAULT_IDS) $(ANSIBLE_INVENTORY)/$(ANSIBLE_INVENTORY_VAULT)