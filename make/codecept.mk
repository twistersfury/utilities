######################################################
# Copyright (C) 2023 Twister's Fury.
# Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
######################################################
# This Makefile is still under development. It could
#   change at any time.
######################################################
# Core Configuration
######################################################
BIN_PATH ?= bin
MAKEFILE_CODECEPT := true
MAKEFILE_JUSTNAME ?= $(firstword $(MAKEFILE_LIST))
MAKEFILE_COMPLETE ?= $(CURDIR)/$(MAKEFILE_JUSTNAME)
MAKE_OPTIONS ?= -f $(MAKEFILE_JUSTNAME)

######################################################
# Default Command
######################################################
.PHONY: test
test:

######################################################
# Include PHP
######################################################
ifndef MAKEFILE_PHP

$(BIN_PATH)/php.mk: | $(BIN_PATH)
	$(MAKEFILE_DOWNLOAD_COMMAND) https://gitlab.com/twistersfury/utilities/-/raw/v2.0.0/make/php.mk

include $(BIN_PATH)/php.mk
endif


######################################################
# Include Composer
######################################################
ifndef MAKEFILE_COMPOSER
	#include https://gitlab.com/twistersfury/utilities/raw/master/make/docker.mk
include $(BIN_PATH)/composer.mk

$(BIN_PATH)/composer.mk: | $(BIN_PATH)
	$(MAKEFILE_DOWNLOAD_COMMAND) https://gitlab.com/twistersfury/utilities/raw/v2.0.0/make/composer.mk
endif


######################################################
# Include Local Overrides
######################################################
# If you don't have wget, then you can manually edit
# 	bin/overrides.mk to enable curl

$(BIN_PATH):
	mkdir -p $@

$(BIN_PATH)/overrides.mk: | $(BIN_PATH)
	echo 'MAKEFILE_OVERRIDES := true' > $@
	echo 'MAKEFILE_DOWNLOAD_COMMAND = wget -O $$@' >> $@
	echo '#MAKEFILE_DOWNLOAD_COMMAND = curl -o $$@' >> $@
	echo "Override File Created, You Will Have To Rerun The Previous Command."
	echo "By default, this script attempts to use wget. If you don't have wget, you can use curl instead."
	echo "If you want to use curl, edit the $@ file to use the curl version of MAKEFILE_DOWNLOAD_COMMAND." && exit 1

# Only Include If Not Already Included (Will Cause Errors Otherwise)
ifndef MAKEFILE_OVERRIDES
	include $(BIN_PATH)/overrides.mk
endif

######################################################
# Default Configurations
######################################################

CODECEPT_IMAGE ?= twistersfury/zephir:phalcon
CODECEPT_OPTIONS ?= --ext DotReporter --fail-fast
CODECEPT_ENV ?=
CODECEPT_PHP_TARGET ?= php
CODECEPT_CONFIG ?= codeception.dist.yml

######################################################
# Core Commands
######################################################

.PHONY: codecept-help
codecept-help: CODECEPT_OPTIONS=""
codecept-help: codecept

.PHONY: codecept-bootstrap
codecept-bootstrap: CODECEPT_COMMAND="bootstrap"
codecept-bootstrap: CODECEPT_OPTIONS=""
codecept-bootstrap: codecept

.PHONY: codecept-build
# Build Codecept
codecept-build: CODECEPT_COMMAND=build
codecept-build: CODECEPT_OPTIONS=
codecept-build: codecept

.PHONY: codecept-gherkin
# Creates Gherkin steps
codecept-gherkin: CODECEPT_COMMAND=gherkin:snippets feature
codecept-gherkin: codecept

.PHONY: codecept-dry-run
# Run test in Dry Mode
codecept-dry-run: CODECEPT_COMMAND=dry-run
codecept-dry-run: codecept

.PHONY: codecept-run
# Run test
codecept-run: DOCKER_EXTRA_OPTIONS=--env ENV_CODECEPT_RUN=true
codecept-run: CODECEPT_COMMAND=run
codecept-run: codecept

.PHONY: codecept
codecept: PHP_IMAGE = $(CODECEPT_IMAGE)
codecept: PHP_FILE=./vendor/codeception/codeception/codecept
codecept: PHP_FILE_ARGS=$(CODECEPT_COMMAND) $(CODECEPT_SUITE) $(CODECEPT_OPTIONS) $(CODECEPT_ARGS)
codecept: vendor/codeception/codeception/codecept $(CODECEPT_CONFIG) $(CODECEPT_PHP_TARGET)


######################################################
# Helper Commands
######################################################

# Run All Testing
.PHONY: test
test: codecept-run

.PHONY: test-coverage
# Run All Testing With Coverage (For Use With Infection & Sonar)
test-coverage:
	$(MAKE) -e codecept-run CODECEPT_ARGS="--xml=junit.xml --coverage-xml=coverage.xml --coverage-html --coverage-phpunit=codeception-coverage-xml/ $(CODECEPT_ARGS)" PHP_VOLUMES="$(PHP_VOLUMES)" CODECEPT_IMAGE=$(CODECEPT_IMAGE) DOCKER_VOLUMES="$(DOCKER_VOLUMES)"

.PHONY: test-unit
# Run Just The Unit Suite
test-unit: CODECEPT_SUITE=unit
test-unit: codecept-run

.PHONY: test-functional
# Run The Functional Suite
test-functional: CODECEPT_SUITE=functional
test-functional: codecept-run

.PHONY: test-acceptance
# Run The Acceptance Suite
test-acceptance: CODECEPT_SUITE=acceptance
test-acceptance: codecept-run

.PHONY: test-feature
# Run The Gherkin Feature Suite
test-feature: CODECEPT_SUITE=feature
test-feature: codecept-run

.PHONY: test-feature-dry
# Run The Feature Suite in dry-mode
test-feature-dry: CODECEPT_SUITE=feature
test-feature-dry: codecept-dry-run

######################################################
# Hard Files
######################################################

tests/_output/junit.xml:
	make test-coverage

tests/_output/codeception-coverage-xml/:
	make test-coverage

vendor/codeception/codeception/codecept: $(CODECEPT_ENV) | vendor
	# Testing Codeception Installed Even Though Vendor Is
	test -a $@
	touch $@

codeception.dist.yml:
	# If this block is being hit, either you should rename `codeception.yml` to `codeception.dist.yml`, or you should set CODECEPT_CONFIG to be `codecept.yml`
	exit 1