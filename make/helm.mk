######################################################
# Copyright (C) 2023 Twister's Fury.
# Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
######################################################
# This Makefile is still under development. It could
#   change at any time.
######################################################
# This Makefile makes the following assumptions:
#   1. There is a defined variable $(BIN_PATH)
#   2. There is a folder named $(BIN_PATH)
######################################################
#                Helm Configuration                #
######################################################

BIN_PATH ?= bin

MAKEFILE_HELM := true
MAKEFILE_JUSTNAME ?= $(firstword $(MAKEFILE_LIST))
MAKEFILE_COMPLETE ?= $(CURDIR)/$(MAKEFILE_JUSTNAME)
MAKE_OPTIONS ?= -f $(MAKEFILE_JUSTNAME)

######################################################
# Default Values
######################################################

HELM_FOLDER ?= helm
HELM_CONFIG ?= values.yaml
HELM_NAME ?= helm
HELM_OPTIONS ?=

HELM_COMMAND_START ?= upgrade --install --wait --timeout 210s
HELM_COMMAND_STOP  ?= uninstall --wait

HELM_DIRECTORY ?= .

######################################################
# Helm Core Commands
######################################################

.PHONY: helm
helm: $(BIN_PATH)/.helm $(BIN_PATH)/.helm_dependencies
	cd $(HELM_FOLDER) && $(GLOBAL_ENV) helm $(HELM_COMMAND) $(HELM_NAME) $(HELM_DIRECTORY)

# Start Docker Images - Shortcut
.PHONY: helm-start
helm-start:
	make $(MAKE_OPTIONS) helm HELM_COMMAND="$(HELM_COMMAND_START) $(HELM_OPTIONS) --values=$(HELM_CONFIG)"

# Stop Docker Images
.PHONY: helm-stop
helm-stop:
	make $(MAKE_OPTIONS) helm HELM_COMMAND="$(HELM_COMMAND_STOP)" HELM_DIRECTORY="" || (echo "Nothing to do" && exit 0)

.PHONY: helm-restart
helm-restart: helm-stop helm-wait helm-start

.PHONY: helm-wait
helm-wait:
	until kubectl get pod 2>&1 | grep "No resources"; do echo waiting for stop; sleep 2; done;

######################################################
# Helm Hard Files
######################################################

$(BIN_PATH)/.helm:
	which helm && touch $@

$(BIN_PATH)/.helm_dependencies: $(HELM_FOLDER)/Chart.yaml
	cd $(HELM_FOLDER) && helm dependency build
	touch $@
