######################################################
# Copyright (C) 2023 Twister's Fury.
# Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
######################################################
# This Makefile is still under development. It could
#   change at any time.
######################################################
# Core Configuration
######################################################
BIN_PATH ?= bin
MAKEFILE_PHPCS := true
MAKEFILE_JUSTNAME ?= $(firstword $(MAKEFILE_LIST))
MAKEFILE_COMPLETE ?= $(CURDIR)/$(MAKEFILE_JUSTNAME)
MAKE_OPTIONS ?= -f $(MAKEFILE_JUSTNAME)

######################################################
# Include Docker
######################################################
ifndef MAKEFILE_DOCKER
include $(BIN_PATH)/docker.mk

$(BIN_PATH)/docker.mk: | $(BIN_PATH)
	$(MAKEFILE_DOWNLOAD_COMMAND) https://gitlab.com/twistersfury/utilities/raw/v2.0.0/make/docker.mk
endif

######################################################
# Include Local Overrides
######################################################
# If you don't have wget, then you can manually edit
# 	bin/overrides.mk to enable curl

$(BIN_PATH):
	mkdir -p $@

$(BIN_PATH)/overrides.mk: | $(BIN_PATH)
	echo 'MAKEFILE_OVERRIDES := true' > $@
	echo 'MAKEFILE_DOWNLOAD_COMMAND = wget -O $$@' >> $@
	echo '#MAKEFILE_DOWNLOAD_COMMAND = curl -o $$@' >> $@
	echo "Override File Created, You Will Have To Rerun The Previous Command."
	echo "By default, this script attempts to use wget. If you don't have wget, you can use curl instead."
	echo "If you want to use curl, edit the $@ file to use the curl version of MAKEFILE_DOWNLOAD_COMMAND." && exit 1

# Only Include If Not Already Included (Will Cause Errors Otherwise)
ifndef MAKEFILE_OVERRIDES
	include $(BIN_PATH)/overrides.mk
endif

######################################################
# Default Configurations
######################################################

CS_FIX_FILE ?= phpcs.xml

######################################################
# Core Commands
######################################################

.PHONY: cs-check
cs-check: DOCKER_COMMAND=run
cs-check: DOCKER_OPTIONS=--rm -v $(CURDIR):/data --workdir=/data
cs-check: DOCKER_IMAGE=cytopia/phpcs
cs-check: DOCKER_ARGS=--standard=$(CS_FIX_FILE) --ignore=vendor/* .
cs-check: docker

.PHONY: cs-fix
cs-fix: DOCKER_COMMAND=run
cs-fix: DOCKER_OPTIONS=--rm -v $(CURDIR):/data --workdir=/data
cs-fix: DOCKER_IMAGE=cytopia/phpcbf
cs-fix: DOCKER_ARGS=--standard=$(CS_FIX_FILE) --ignore=vendor/* .
cs-fix: docker