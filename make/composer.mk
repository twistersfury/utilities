######################################################
# Copyright (C) 2023 Twister's Fury.
# Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
######################################################
# This Makefile is still under development. It could
#   change at any time.
######################################################
# Core Configuration
######################################################
BIN_PATH ?= bin
MAKEFILE_COMPOSER := true
MAKEFILE_JUSTNAME ?= $(firstword $(MAKEFILE_LIST))
MAKEFILE_COMPLETE ?= $(CURDIR)/$(MAKEFILE_JUSTNAME)
MAKE_OPTIONS ?= -f $(MAKEFILE_JUSTNAME)

######################################################
# Include Docker
######################################################
ifndef MAKEFILE_DOCKER
include $(BIN_PATH)/docker.mk

$(BIN_PATH)/docker.mk: | $(BIN_PATH)
	$(MAKEFILE_DOWNLOAD_COMMAND) https://gitlab.com/twistersfury/utilities/-/raw/v2.0.0/make/docker.mk
endif

######################################################
# Include PHP
######################################################
ifndef MAKEFILE_PHP
include $(BIN_PATH)/php.mk

$(BIN_PATH)/php.mk: | $(BIN_PATH)
	$(MAKEFILE_DOWNLOAD_COMMAND) https://gitlab.com/twistersfury/utilities/-/raw/v2.0.0/make/php.mk
endif

######################################################
# Include Local Overrides
######################################################
# If you don't have wget, then you can manually edit
# 	bin/overrides.mk to enable curl

$(BIN_PATH):
	mkdir -p $@

$(BIN_PATH)/overrides.mk: | $(BIN_PATH)
	echo 'MAKEFILE_OVERRIDES := true' > $@
	echo 'MAKEFILE_DOWNLOAD_COMMAND = wget -O $$@' >> $@
	echo '#MAKEFILE_DOWNLOAD_COMMAND = curl -o $$@' >> $@
	echo "Override File Created, You Will Have To Rerun The Previous Command."
	echo "By default, this script attempts to use wget. If you don't have wget, you can use curl instead."
	echo "If you want to use curl, edit the $@ file to use the curl version of MAKEFILE_DOWNLOAD_COMMAND." && exit 1

# Only Include If Not Already Included (Will Cause Errors Otherwise)
ifndef MAKEFILE_OVERRIDES
	include $(BIN_PATH)/overrides.mk
endif

######################################################
# Default Configurations
######################################################

COMPOSER_IMAGE ?= twistersfury/composer

# Define COMPOSER_IGNORE_PLATFORM_REQS To Ignore Platform Reqs (Not Recommended, Use COMPOSER_IMAGE Instead)
#COMPOSER_IGNORE_PLATFORM_REQS ?= --ignore-platform-reqs

######################################################
#                 Composer Core Commands             #
######################################################

.PHONY: composer-install
composer-install: COMPOSER_COMMAND=install
composer-install: COMPOSER_OPTIONS ?= $(COMPOSER_IGNORE_PLATFORM_REQS)
composer-install: composer

.PHONY: composer-dump-autoload
composer-dump-autoload: COMPOSER_COMMAND=dump-autoload
composer-dump-autoload: COMPOSER_OPTIONS=
composer-dump-autoload: composer

.PHONY: composer-update
composer-update: COMPOSER_COMMAND=update $(COMPOSER_PACKAGE)
composer-update: COMPOSER_OPTIONS=$(COMPOSER_IGNORE_PLATFORM_REQS)
composer-update: composer

.PHONY: composer-require
composer-require: COMPOSER_COMMAND=require $(COMPOSER_PACKAGE)
composer-require: COMPOSER_OPTIONS=$(COMPOSER_IGNORE_PLATFORM_REQS)
composer-require: composer


.PHONY: composer-update-static
composer-update-static: DOCKER_NETWORK=
composer-update-static: DOCKER_IMAGE=$(COMPOSER_IMAGE)
composer-update-static: APP_DIRECTORY=/app
composer-update-static: DOCKER_NETWORK=
composer-update-static:
	$(DOCKER_COMMAND) composer-update-static

.PHONY: composer
composer: PHP_IMAGE=$(COMPOSER_IMAGE)
composer: DOCKER_IMAGE=$(COMPOSER_IMAGE)
composer: APP_DIRECTORY=/app
composer: DOCKER_NETWORK_ARGS=
composer: PHP_FILE=//usr/bin/composer
composer: PHP_FILE_ARGS=$(COMPOSER_COMMAND) $(COMPOSER_OPTIONS)
composer: PHP_EXTRA_VOLUMES=--volume "$$HOME/.composer/auth.json":/composer/auth.json
composer: $(BIN_PATH)/.docker composer.json php

######################################################
#          Composer Hard File Requirements           #
######################################################

composer.lock: | vendor
	test -a composer.lock
	touch $@

vendor:
	make composer-install
	touch $@
