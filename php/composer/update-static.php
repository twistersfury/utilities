#!/usr/bin/env php
<?php
/**
 * Raise/update static version numbers in composer.json.
 *
 * Run on the CLI: "composer outdated --direct > outdated.txt"
 *
 * Original: https://stackoverflow.com/questions/41200946/update-composer-dependencies-to-latest-despite-static-version-numbers
 */

$checkMode = false;
$excludes = [];

$totalArgs = count($argv);
for ($arg = 0; $arg < $totalArgs; $arg++) {
    switch($argv[$arg]) {
        case '--check':
            $checkMode = true;
            break;
        case '--exclude':
            $excludes[] = $argv[++$arg];
            break;
        case '--help':
            echo 'Composer Package Version Updater' . "\r\n";
            echo '================================' . "\r\n";
            echo "\r\n";
            echo "Reads the composer.json file and compares it to `composer outdated`, updating any packages that have newer versions and rebuilding the composer.json file.\r\n";
            echo "\r\n";
            echo "Options:\r\n";
            echo "  --check: Run In Check Mode (For Use In CI/CD)\r\n";
            echo "  --exclude package/name: Will Exclude 'package/name' package from updates. Can be used more than once.\r\n";
            echo "  --help: Display this help.\r\n";
            exit(3);

    }
}

$totalPackages = 0;

$composerJson = json_decode(file_get_contents('composer.json'), true);

$output = null;
$response = null;
exec('composer outdated --direct > composer-outdated.log', $output, $response);
if ($response !== 0) {
    echo "Failed To Get Package List. Please run `composer install` and resolve the issues.";
    exit(2);
}

$listOfOutdatedPackages = file('composer-outdated.log');

foreach ($listOfOutdatedPackages as $line) {
    $regexp = '/(?P<package>[\w]+\/[\w]+).*(?P<currentVersion>\d+\.\d+\.\d+).*(?P<latestVersion>\d+\.\d+\.\d+)/';
    preg_match($regexp, $line, $matches);
    $matches = array_filter($matches, 'is_string', ARRAY_FILTER_USE_KEY);

    if (isset($matches['package']) && !in_array($matches['package'], $excludes)) {
        $package = $matches['package'];

        foreach (['require', 'require-dev'] as $type) {
            $currentVersion = $composerJson[$type][$package] ?? $matches['latestVersion'];
            if ($currentVersion === $matches['latestVersion']) {
                continue;
            }

            ++$totalPackages;

            echo sprintf(
                'Updating %s from %s to %s' . "\n",
                $package,
                $currentVersion,
                $matches['latestVersion']
            );

            $composerJson[$type][$package] = $matches['latestVersion'];
        }
    }
}

if (!$checkMode && $totalPackages > 0) {
    file_put_contents(
        'composer.json',
        json_encode(
            $composerJson,
            JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
        )
    );

    exit(0);
}

if ($totalPackages === 0) {
    echo "All Good! No Packages To Update.";

    exit(0);
} else {
    echo "$totalPackages Package Updates";
    exit(1);
}